"use strict";


// 2. Напишіть функцію greeting, яка приймає об'єкт з властивостями name та age, 
// і повертає рядок з привітанням і віком,
// наприклад "Привіт, мені 30 років".
// Попросіть користувача ввести своє ім'я та вік
// за допомогою prompt, і викличте функцію gteeting з введеними даними(передавши їх як аргументи). 
// Результат виклику функції виведіть з допомогою alert.




let username = prompt("Enter your name:");
let userAge = prompt("Enter your age:");

function Greeting(username,userAge){
    this.username = username;
    this.userAge = userAge;

    this.greet = function(){
        return`Hi, I am ${this.username} and I am ${this.userAge}.`;
    };
};

const newUser = new Greeting(username, userAge);
alert(newUser.greet());

